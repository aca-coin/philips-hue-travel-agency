<section class="information">
    <#if image?? && image.getData()?? && image.getData() != "">
        <div class="banner" style="background-image: url('${image.getData()}')"></div>
    </#if>
    <div class="container">
        ${content.getData()}
    <div>
</section>