<#assign math = staticUtil["java.lang.Math"] />
<#assign root_folder = theme_display.getPathThemeRoot() >
<#assign RoleLocalService = serviceLocator.findService("com.liferay.portal.kernel.service.RoleLocalService")>
<#assign hasAdminRole = RoleLocalService.hasUserRole(user_id, company_id, "Administrator", false)>

<#function getPageClass>
    <#if is_first_child >
        <#return "home">
    <#else>
        <#return "detail">
    </#if>
</#function>