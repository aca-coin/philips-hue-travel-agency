<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
<#assign articleId = .vars['reserved-article-id'].data />
<#assign article = journalArticleLocalService.getArticle(groupId, articleId)>

<#if name?? && Country??>
    <#assign photoUrl = "" />
    <#if photo?? && photo.getSiblings()?has_content>
        <#assign cur_photo = photo.getSiblings()?first >
        <#if cur_photo.getData()?? && cur_photo.getData() != "">
            <#assign photoUrl = cur_photo.getData() />
        </#if>
    </#if>


    <article>
        <div class="image" style="background-image: url('${photoUrl}');">
			<div class="icon"></div>
        </div>
        <div class="text">
            <h3>
                <span>${name.getData()}</span>
                <#assign starCount = stars.getData()?number >
                <#list 1..starCount as i >
                    <i class="fa fa-star" aria-hidden="true"></i>
                </#list>
            </h3>
            <h4>${Country.getData()}</h4>
            <p>
                <#if (shortDescription.getData()?length > 200) >
                    ${shortDescription.getData()?substring(0, 200)}...
                <#else>
                    ${shortDescription.getData()}
                </#if>
            </p>

            <#if feature.getSiblings()?has_content>
                <ul>
                    <#list feature.getSiblings() as cur_feature>
                        <#if cur_feature.getData()?trim != "">
                            <li>${cur_feature.getData()}</li>
                        </#if>
                    </#list>
                </ul>
            </#if>

            <a href="/-/${article.getUrlTitle()}">View <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
        </div>
    </article>
</#if>
