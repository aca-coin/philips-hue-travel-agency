<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ACA Light Travel</title>

    <link rel="apple-touch-icon" href="${images_folder}/apple-touch-icon.png">

    <link rel="stylesheet" href="${css_folder}/roboto-fontface.css" />
    <link rel="stylesheet" href="${css_folder}/font-awesome.css" />

    <script src="${javascript_folder}/modernizr/modernizr.js"></script>

	<@liferay_util["include"] page=top_head_include />
</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />


<div id="container" class="${getPageClass()}">

    <#if hasAdminRole >
        <@liferay.control_menu />
    </#if>

    <header>
        <nav>
            <div class="menu">
                <button type="button" onclick="openMenu()">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                    <spam>Menu</spam>
                </button>
            </div>

            <div class="profile">

				<#if !is_signed_in>
					<a data-redirect="${is_login_redirect_required?string}" href="${sign_in_url}" id="sign-in" rel="nofollow">Login</a>
				<#else>
                    <label>${user_name}</label>
                    <i class="fa fa-user" aria-hidden="true"></i>
				</#if>

            </div>
        </nav>

        <div class="menu-container" id="menu">
            <div class="menu">
                <button type="button" onclick="closeMenu()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                    <spam>Close</spam>
                </button>
            </div>
            <div class="logo">
                <img src="${images_folder}/logo.png" />
            </div>
            <ul>
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="/#promotions">Promotions</a>
                </li>
                <li>
                    <a href="/#popular">Popular destinations</a>
                </li>
                <li>
                    <a href="/#others">Other destinations</a>
                </li>
                <li>
                    <a href="/#contact">Contact</a>
                </li>
            </ul>

            <div class="social">
                <a href="#">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
                <a href="#">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
                <a href="#">
                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                </a>
                <a href="#">
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                </a>
            </div>
        </div>

        <#if is_first_child >
            <div class="banner" style="background-image: url('${images_folder}/header-${math.floor(math.random() * ((5-1)+1) + 1)}.jpg');">
                <img src="${images_folder}/logo.png" />
                <h1>Follow the lights to the best travel destinations</h1>
            </div>
        </#if>
    </header>

    <div id="content">

		<#if selectable>
				<@liferay_util["include"] page=content_include />
			<#else>
		${portletDisplay.recycle()}

		${portletDisplay.setTitle(the_title)}

			<@liferay_theme["wrap-portlet"] page="portlet.ftl">
				<@liferay_util["include"] page=content_include />
			</@>
		</#if>

    </div>

    <footer id="contact">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <img src="${images_folder}/logo.png" />
                </div>
                <div class="col-md-6 right">
                    <h2>ACA IT-Solutions</h2>
                    <h3><strong>Office Hasselt</strong></h3>
                    <p>
                        Herkenrodesingel 8B 2.01<br/>
                        B-3500 Hasselt<br/>
                        <i class="fa fa-phone" aria-hidden="true"></i> +32(0)11 26.50.10<br/>
                        <i class="fa fa-envelope" aria-hidden="true"></i> info@aca-it.be<br/>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="copy">
                        Copyright &copy; 2017 ACA IT-Solutions. All rights reserved.
                    </div>
                </div>
                <div class="col-md-6 right">
                    <div class="social">
                        <a href="#">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-google-plus" aria-hidden="true"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

<script src="${javascript_folder}/modernizr/modernizr.js"></script>

</body>

</html>