<#assign journalArticleLocalService = serviceLocator.findService("com.liferay.journal.service.JournalArticleLocalService")>
<#assign articleId = .vars['reserved-article-id'].data />
<#assign article = journalArticleLocalService.getArticle(groupId, articleId)>

<#if name?? && Country??>
    <#assign photoUrl = "" />
    <#if photo.getSiblings()?has_content>
        <#assign cur_photo = photo.getSiblings()?first >
        <#if cur_photo.getData()?? && cur_photo.getData() != "">
            <#assign photoUrl = cur_photo.getData() />
        </#if>
    </#if>

    <a href="/-/${article.getUrlTitle()}">
        <article>
            <div class="image" style="background-image: url('${photoUrl}');">
                <div class="discount">${discount.getData()}%</div>
				<div class="icon"></div>
            </div>
            <div class="text">
                <h3>
                    <span>${name.getData()}</span>
                    <#assign starCount = stars.getData()?number >
                    <#list 1..starCount as i >
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </#list>
                </h3>
            </div>
        </article>
    </a>
</#if>