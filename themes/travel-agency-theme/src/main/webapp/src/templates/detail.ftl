<section class="detail">
    <div class="banner" style="background-image: url('/images/photos/dave-poore-37828.jpg');">
        <div id="photoCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">

                <#if photo.getSiblings()?has_content>
                    <#list photo.getSiblings() as cur_photo>
                        <#if cur_photo.getData()?? && cur_photo.getData() != "">
                            <#assign cssStyle = "" >
                            <#if cur_photo?is_first >
                                <#assign cssStyle = "active" >
                            </#if>
                            <li data-target="#photoCarousel" data-slide-to="${cur_photo?index}" class="${cssStyle}"></li>
                        </#if>
                    </#list>
                </#if>

            </ol>
            <div class="carousel-inner" role="listbox">

                <#if photo.getSiblings()?has_content>
                    <#list photo.getSiblings() as cur_photo>
                        <#if cur_photo.getData()?? && cur_photo.getData() != "">
                            <#assign cssStyle = "" >
                            <#if cur_photo?is_first >
                                <#assign cssStyle = "active" >
                            </#if>
                            <div class="item ${cssStyle}" style="background-image: url('${cur_photo.getData()}');"></div>
                        </#if>
                    </#list>
                </#if>

            </div>
            <a class="left carousel-control" href="#photoCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#photoCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>


        </div>
        <h1>
            <span>${name.getData()}</span>
            <#assign starCount = stars.getData()?number >
            <#list 1..starCount as i >
                <i class="fa fa-star" aria-hidden="true"></i>
            </#list>
        </h1>
        <h3 class="text-center">${Country.getData()}</h3>

    </div>

    <div class="container">
        <div class="col-md-6 text-left">
            <a href="/" class="button" >
                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back to overview
            </a>
        </div>
        <div class="col-md-6 text-right">

            <div class="social">
                <a href="#"> <i class="fa fa-facebook" aria-hidden="true"></i> </a>
                <a href="#"> <i class="fa fa-twitter" aria-hidden="true"></i> </a>
                <a href="#"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a>
                <a href="#"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a>
            </div>

            <a href="/success" class="button filled" >
                Book now
            </a>
        </div>
        <div class="col-md-12">

            <div class="short">${shortDescription.getData()}</div>
            <div class="long">${longDescription.getData()}</div>

            <#if feature.getSiblings()?has_content>
                <h3 class="text-left">Facilities and Services</h3>
                <ul>
                    <#list feature.getSiblings() as cur_feature>
                        <#if cur_feature.getData()?trim != "">
                            <li>${cur_feature.getData()}</li>
                        </#if>
                    </#list>
                </ul>
            </#if>

            <div class="text-center">
                <a href="/" class="button" >
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back to overview
                </a>
            </div>
        </div>

    </div>

</section>