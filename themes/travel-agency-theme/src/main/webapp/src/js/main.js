AUI().ready(

	function() {
	}
);

Liferay.Portlet.ready(

	function(portletId, node) {
	}
);

Liferay.on(
	'allPortletsReady',

	function() {
	}
);


$(document).ready(function () {

});

// Global javascript functions

function openMenu() {
    $('#menu').addClass('open');
    $('body').css('overflow', 'hidden');
}

function closeMenu() {
    $('#menu').removeClass('open');
    $('body').css('overflow', 'initial');
}

function loadMore(type, count, buttonId) {
    var list = $('li.item-' + type +'.hide');

    if (list.length <= count) {
    	$('#' + buttonId).hide();
	}

	list.slice(0,count).each(function(index) {
        $( this ).removeClass('hide');
    });
}