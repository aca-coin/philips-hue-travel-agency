package be.aca.coin.hue.travel.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.*;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.PropsUtil;
import com.philips.lighting.hue.sdk.PHAccessPoint;
import com.philips.lighting.hue.sdk.PHBridgeSearchManager;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.hue.sdk.PHSDKListener;
import com.philips.lighting.hue.sdk.utilities.PHUtilities;
import com.philips.lighting.hue.sdk.utilities.impl.Color;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHLightState;

@Component(
		immediate = true,
		property = {
				"com.liferay.portlet.display-category=Travel Agency",
				"com.liferay.portlet.instanceable=true",
				"com.liferay.portlet.css-class-wrapper=travel-agency-portlet",
				"javax.portlet.name=travelAgencyPortlet",
				"javax.portlet.display-name=Travel Agency Portlet",
				"javax.portlet.init-param.template-path=/",
				"javax.portlet.init-param.view-template=/view.jsp",
				"javax.portlet.security-role-ref=power-user,user" },
		service = Portlet.class)
public class TravelAgencyPortlet extends MVCPortlet {

	private static final String BRIDGE_IP_KEY = "hue.bridge.ip";
	private static final String BRIDGE_PORT_KEY = "hue.bridge.port";
	private static final String BRIDGE_USER_NAME_KEY = "hue.bridge.userName";
	private static final String FIRST_GROUP = "1";

	private PHSDKListener listener = new PHSDKListener() {

		public void onAccessPointsFound(List accessPoint) {
			System.out.println("Found some bridges... " +accessPoint.size());
			for (PHAccessPoint accesPoint : (List<PHAccessPoint>)accessPoint) {
				System.out.println("IP: " + accesPoint.getIpAddress());
				System.out.println("userName: " + accesPoint.getUsername());
				System.out.println("bridgeID: " + accesPoint.getBridgeId());
			}

			phHueSDK.connect((PHAccessPoint) accessPoint.get(0));
			System.out.println("connected...");
		}

		public void onCacheUpdated(List cacheNotificationsList, PHBridge bridge) {}

		public void onBridgeConnected(PHBridge b, String username) {
			System.out.println("Connected with username: " + username);
			phHueSDK.setSelectedBridge(b);
		}

		public void onAuthenticationRequired(PHAccessPoint accessPoint) {
			System.out.println("Push the button!");
			phHueSDK.startPushlinkAuthentication(accessPoint);
		}

		public void onConnectionResumed(PHBridge bridge) {}

		public void onConnectionLost(PHAccessPoint accessPoint) {}

		public void onError(int code, final String message) {}

		public void onParsingErrors(List parsingErrorsList) {}
	};

	private boolean lightsOn = true;
	private PHHueSDK phHueSDK;

	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		connectToBridge();
		// The following is not possible as after each action the doView is executed again
		//		switchLightsInGroup(true, FIRST_GROUP);

		super.doView(renderRequest, renderResponse);
	}

	public void switchLights(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
		lightsOn = !lightsOn;

		switchLightsInGroup(lightsOn, FIRST_GROUP);
	}

	private void switchLightsInGroup(boolean lightsOn, String group) {
		PHBridge bridge = phHueSDK.getSelectedBridge(); //PHHueSDK.getInstance().getSelectedBridge();
		PHLightState lightState = new PHLightState();
		lightState.setOn(lightsOn);

		bridge.setLightStateForGroup(group, lightState);
	}

	private void connectToBridge() {
		if (phHueSDK == null) {
			phHueSDK = PHHueSDK.create();

			/* EMULATOR */
			// Try to connect to emulator
			String bridgeIp = PropsUtil.get(BRIDGE_IP_KEY);
			String bridgeUserName = PropsUtil.get(BRIDGE_USER_NAME_KEY);

			PHAccessPoint lastAccessPoint = new PHAccessPoint();
			lastAccessPoint.setIpAddress(bridgeIp);
			lastAccessPoint.setUsername(bridgeUserName);
			phHueSDK.connect(lastAccessPoint);

			/* REAL BRIDGE */
			phHueSDK.setAppName("TravelAgency");
			phHueSDK.setDeviceName("Kiosk");

			// create & register Listener to provide all callbacks
			phHueSDK.getNotificationManager().registerSDKListener(listener);

			// start bridge search
			PHBridgeSearchManager sm = (PHBridgeSearchManager) phHueSDK.getSDKService(PHHueSDK.SEARCH_BRIDGE);
			sm.search(true, true);
		}
	}

	public void changeColor(ActionRequest actionRequest, ActionResponse actionResponse) {
		String selectedColor = actionRequest.getParameter("selectedColor");

		int color = Color.parseColor(selectedColor); // this is an impl class!
		float[] xy = PHUtilities.calculateXY(color, " ");

		PHBridge bridge = phHueSDK.getSelectedBridge();
		PHLightState lightState = new PHLightState();
		lightState.setX(xy[0]);
		lightState.setY(xy[1]);

		//		lightState.setHue(Color.rgb( 67, 113, 55));

		bridge.setLightStateForGroup(FIRST_GROUP, lightState);
	}
}