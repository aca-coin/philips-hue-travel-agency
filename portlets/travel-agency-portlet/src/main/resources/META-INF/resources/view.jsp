<%@ include file="/init.jsp" %>

<portlet:actionURL name="switchLights" var="switchLightsURL" />
<portlet:actionURL name="changeColor" var="changeColorURL" />

<div id="<portlet:namespace/>tavel-agency">Welcome to our example Travel Agency!</div>

<aui:form action="<%= switchLightsURL %>">
	<aui:button type="submit" value="Switch lights"/>
</aui:form>

<input type="color" name="selectedColor" id="<portlet:namespace/>-color-input">
<aui:form action="<%= changeColorURL %>">
	<aui:input type="hidden" name="selectedColor" id="selected-color" value="black" />
	<aui:button type="submit" value="Change color"/>
</aui:form>

<aui:script>
	$('#<portlet:namespace/>-color-input').on('change', function() {
		var selectedColor = $(this).val();
		$('#<portlet:namespace/>selected-color').val(selectedColor);
	});
</aui:script>