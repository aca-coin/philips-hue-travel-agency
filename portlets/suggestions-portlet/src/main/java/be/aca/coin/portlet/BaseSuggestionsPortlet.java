package be.aca.coin.portlet;

import be.aca.coin.portlet.domain.ContentItem;
import be.aca.coin.segment.service.UserSegmentService;
import com.liferay.asset.kernel.model.AssetCategory;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.content.targeting.model.UserSegment;
import com.liferay.content.targeting.model.UserSegmentModel;
import com.liferay.content.targeting.util.ContentTargetingUtil;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleDisplay;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.WebKeys;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseSuggestionsPortlet extends MVCPortlet {

	private static final String ATTRIBUTE_SERVLET_REQUEST = "com.liferay.portal.kernel.servlet.PortletServletRequest";
	private static final String ATTRIBUTE_SERVLET_RESPONSE = "com.liferay.portal.kernel.servlet.PortletServletResponse";
	protected static final String ATTRIBUTE_ENTRIES = "entries";
	protected static final String ATTRIBUTE_VIEW = "view";

	protected static final String ARTICLE_CLASS_NAME = "com.liferay.journal.model.JournalArticle";
	private static final String CATEGORY_PROMOTION = "Promotion";

	private static final String COLUMN_NAME = "name";
	private static final String COLUMN_CATEGORY_ID = "categoryId";

	private static final String STRUCTURE_KEY_JOURNEY = "JOURNEY";

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		try {
			HttpServletRequest httpServletRequest = (HttpServletRequest) renderRequest.getAttribute(ATTRIBUTE_SERVLET_REQUEST);
			HttpServletResponse httpServletResponse = (HttpServletResponse) renderRequest.getAttribute(ATTRIBUTE_SERVLET_RESPONSE);
			ThemeDisplay themeDisplay = (ThemeDisplay) httpServletRequest.getAttribute(WebKeys.THEME_DISPLAY);

			long[] categoryIds = getCategoryIds(httpServletRequest, httpServletResponse, themeDisplay);
			List<AssetEntry> entries = findAssetEntries(categoryIds);
			List<ContentItem> content = convertAssetEntriesToContent(entries, themeDisplay);

			renderRequest.setAttribute(ATTRIBUTE_ENTRIES, content);
			renderRequest.setAttribute(ATTRIBUTE_VIEW, getView());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		super.doView(renderRequest, renderResponse);
	}

	private long[] getCategoryIds(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, ThemeDisplay themeDisplay) throws Exception {
		List<UserSegment> userSegments = getUserSegmentIdService().getUserSegments(httpServletRequest, httpServletResponse);
		long[] segmentIds = userSegments.stream().map(UserSegmentModel::getUserSegmentId).mapToLong(Long::longValue).toArray();

		long[] ids = ContentTargetingUtil.getAssetCategoryIds(themeDisplay.getScopeGroupId(), segmentIds);

		return ids;
	}

	protected long getPromotionCategoryId() {
		DynamicQuery query = getAssetCategoryService().dynamicQuery();
		query.add(RestrictionsFactoryUtil.eq(COLUMN_NAME, CATEGORY_PROMOTION));
		query.setProjection(ProjectionFactoryUtil.property(COLUMN_CATEGORY_ID));
		List<Object> objects = getAssetCategoryService().dynamicQuery(query);

		return (long) objects.get(0);
	}

	private List<ContentItem> convertAssetEntriesToContent(List<AssetEntry> entries, ThemeDisplay themeDisplay) throws PortalException {
		List<ContentItem> content = new ArrayList<>();

		for (AssetEntry entry : entries) {
			JournalArticle journalArticle = getJournalArticleService().getArticlesByResourcePrimKey(entry.getClassPK()).get(0);

			if (STRUCTURE_KEY_JOURNEY.equals(journalArticle.getDDMStructureKey())) {
				JournalArticleDisplay articleDisplay = getJournalArticleService().getArticleDisplay(journalArticle.getGroupId(), journalArticle.getArticleId(), getDDMTemplateKey(), "", LocaleUtil.toLanguageId(themeDisplay.getLocale()), themeDisplay);
				String html = articleDisplay.getContent();

				String travelCategory = findTravelCategory(entry.getCategories());

				content.add(new ContentItem(html, travelCategory));
			}
		}

		return content;
	}

	private String findTravelCategory(List<AssetCategory> categories) {
		for (AssetCategory category : categories) {
			if (!CATEGORY_PROMOTION.equals(category.getName())) {
				return category.getName().toLowerCase();
			}
		}

		return "";
	}

	protected abstract List<AssetEntry> findAssetEntries(long[] categoryIds);

	protected abstract UserSegmentService getUserSegmentIdService();

	protected abstract AssetEntryLocalService getAssetEntryService();

	protected abstract AssetCategoryLocalService getAssetCategoryService();

	protected abstract JournalArticleLocalService getJournalArticleService();

	protected abstract String getView();

	protected abstract String getDDMTemplateKey();
}
