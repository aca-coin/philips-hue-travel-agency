package be.aca.coin.portlet;

import be.aca.coin.segment.service.UserSegmentService;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.portal.kernel.util.ArrayUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.Portlet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component(
		immediate = true,
		property = {
				"com.liferay.portlet.display-category=Travel Agency",
				"com.liferay.portlet.instanceable=true",
				"com.liferay.portlet.css-class-wrapper=other-suggestions-portlet",
				"javax.portlet.name=otherSuggestionsPortlet",
				"javax.portlet.display-name=Other Suggestions Portlet",
				"javax.portlet.init-param.template-path=/",
				"javax.portlet.init-param.view-template=/view.jsp",
				"javax.portlet.security-role-ref=power-user,user"},
		service = Portlet.class)
public class OtherSuggestionsPortlet extends BaseSuggestionsPortlet {

	@Reference
	private UserSegmentService userSegmentIdService;

	@Reference
	private AssetEntryLocalService assetEntryService;

	@Reference
	private AssetCategoryLocalService assetCategoryService;

	@Reference
	private JournalArticleLocalService journalArticleService;

	protected List<AssetEntry> findAssetEntries(long[] categoryIds) {
		long promotionCategoryId = getPromotionCategoryId();

		AssetEntryQuery entryQuery = new AssetEntryQuery();
		entryQuery.setNotAnyCategoryIds(ArrayUtil.append(categoryIds, promotionCategoryId));
		entryQuery.setClassName(ARTICLE_CLASS_NAME);
		entryQuery.setEnablePermissions(true);

		List<AssetEntry> entries = getAssetEntryService().getEntries(entryQuery);
		List<AssetEntry> result = new ArrayList<>(entries);
		Collections.shuffle(result);

		return result;
	}

	protected UserSegmentService getUserSegmentIdService() {
		return userSegmentIdService;
	}

	protected AssetEntryLocalService getAssetEntryService() {
		return assetEntryService;
	}

	@Override
	protected AssetCategoryLocalService getAssetCategoryService() {
		return assetCategoryService;
	}

	@Override
	protected JournalArticleLocalService getJournalArticleService() {
		return journalArticleService;
	}

	@Override
	protected String getView() {
		return "other";
	}

	@Override
	protected String getDDMTemplateKey() {
		return "OTHERS-DETAIL";
	}
}