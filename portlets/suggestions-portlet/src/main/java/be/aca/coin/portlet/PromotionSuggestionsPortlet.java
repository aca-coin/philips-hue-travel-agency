package be.aca.coin.portlet;

import be.aca.coin.segment.service.UserSegmentService;
import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.asset.kernel.service.AssetCategoryLocalService;
import com.liferay.asset.kernel.service.AssetEntryLocalService;
import com.liferay.asset.kernel.service.persistence.AssetEntryQuery;
import com.liferay.journal.service.JournalArticleLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.Portlet;
import java.util.*;

@Component(
		immediate = true,
		property = {
				"com.liferay.portlet.display-category=Travel Agency",
				"com.liferay.portlet.instanceable=true",
				"com.liferay.portlet.css-class-wrapper=promotion-suggestions-portlet",
				"javax.portlet.name=promotionSuggestionsPortlet",
				"javax.portlet.display-name=Promotion Suggestions Portlet",
				"javax.portlet.init-param.template-path=/",
				"javax.portlet.init-param.view-template=/view.jsp",
				"javax.portlet.security-role-ref=power-user,user"},
		service = Portlet.class)
public class PromotionSuggestionsPortlet extends BaseSuggestionsPortlet {

	@Reference
	private UserSegmentService userSegmentIdService;

	@Reference
	private AssetEntryLocalService assetEntryService;

	@Reference
	private AssetCategoryLocalService assetCategoryService;

	@Reference
	private JournalArticleLocalService journalArticleService;

	@Override
	protected List<AssetEntry> findAssetEntries(long[] categoryIds) {
		Set<AssetEntry> destinations = new LinkedHashSet<>();

		List<AssetEntry> userPromotionAssetEntries = findUserPromotionAssetEntries(categoryIds);
		ArrayList<AssetEntry> userPromotionAssetEntriesList = new ArrayList<>(userPromotionAssetEntries);
		Collections.shuffle(userPromotionAssetEntriesList);
		destinations.addAll(userPromotionAssetEntriesList);

		List<AssetEntry> otherPromotionAssetEntries = findOtherPromotionAssetEntries(categoryIds);
		ArrayList<AssetEntry> otherPromotionAssetEntriesList = new ArrayList<>(otherPromotionAssetEntries);
		Collections.shuffle(otherPromotionAssetEntriesList);
		destinations.addAll(otherPromotionAssetEntriesList);

		ArrayList<AssetEntry> result = new ArrayList<>();
		result.addAll(destinations);

		return result;
	}

	private List<AssetEntry> findUserPromotionAssetEntries(long [] categoryIds) {
		long promotionCategoryId = getPromotionCategoryId();

		AssetEntryQuery entryQuery = new AssetEntryQuery();
		entryQuery.setAnyCategoryIds(categoryIds);
		entryQuery.setAllCategoryIds(new long[]{promotionCategoryId});
		entryQuery.setClassName(ARTICLE_CLASS_NAME);
		entryQuery.setEnablePermissions(true);

		return getAssetEntryService().getEntries(entryQuery);
	}

	private List<AssetEntry> findOtherPromotionAssetEntries(long [] categoryIds) {
		long promotionCategoryId = getPromotionCategoryId();

		AssetEntryQuery entryQuery = new AssetEntryQuery();
		entryQuery.setNotAnyCategoryIds(categoryIds);
		entryQuery.setAllCategoryIds(new long[]{promotionCategoryId});
		entryQuery.setClassName(ARTICLE_CLASS_NAME);
		entryQuery.setEnablePermissions(true);

		return getAssetEntryService().getEntries(entryQuery);
	}

	protected UserSegmentService getUserSegmentIdService() {
		return userSegmentIdService;
	}

	protected AssetEntryLocalService getAssetEntryService() {
		return assetEntryService;
	}

	@Override
	protected AssetCategoryLocalService getAssetCategoryService() {
		return assetCategoryService;
	}

	@Override
	protected JournalArticleLocalService getJournalArticleService() {
		return journalArticleService;
	}

	@Override
	protected String getView() {
		return "promotion";
	}

	@Override
	protected String getDDMTemplateKey() {
		return "PROMOTION-DETAIL";
	}
}
