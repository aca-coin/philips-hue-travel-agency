package be.aca.coin.portlet.domain;

public class ContentItem {

	private String content;
	private String category;

	public ContentItem(String content, String category) {
		this.content = content;
		this.category = category;
	}

	public String getContent() {
		return content;
	}
	
	public String getCategory() {
		return category;
	}
}
