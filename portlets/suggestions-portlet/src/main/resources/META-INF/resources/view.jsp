<%@ page import="java.util.List" %>
<%@ page import="be.aca.coin.portlet.domain.ContentItem" %>
<%@ include file="/init.jsp" %>

<%
    String view = (String) request.getAttribute("view");
    List<ContentItem> entries = (List<ContentItem>) request.getAttribute("entries");
%>

<jsp:include page="views/${view}.jsp" flush="true" />