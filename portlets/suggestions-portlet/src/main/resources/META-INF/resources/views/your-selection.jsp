<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<section class="popular" id="popular" style="background-image: url('/o/travel-agency-theme/images/bg-${entries[0].category}.jpg');">
    <div class="cover">
        <div class="container">
            <h2>Your most popular travel destinations</h2>

            <ul class="list">
                <c:forEach items="${entries}" var="entry" varStatus="loop">
                    <c:choose>
                        <c:when test="${loop.index < 2}">
                            <li class="item-${loop.index} ${entry.category} item-popular">
                        </c:when>
                        <c:otherwise>
                            <li class="item-${loop.index} ${entry.category} item-popular hide">
                        </c:otherwise>
                    </c:choose>
                        ${entry.content}
                    </li>
                </c:forEach>
            </ul>

            <div class="text-center">
                <button type="button" id="morePopularButton" class="link" onclick="loadMore('popular', 2, 'morePopularButton')">
                    Load more <i class="fa fa-arrow-down" aria-hidden="true"></i>
                </button>
            </div>

        </div>
    </div>
</section>