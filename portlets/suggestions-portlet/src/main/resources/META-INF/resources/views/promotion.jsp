<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<section class="promotions" id="promotions">
    <div class="container">
        <h2>Top destinations and promotions</h2>
        <ul>
            <c:forEach items="${entries}" var="entry" varStatus="loop">
                <c:choose>
                    <c:when test="${loop.index < 4}">
                        <li class="item-${loop.index} ${entry.category} item-popular">
                    </c:when>
                    <c:otherwise>
                        <li class="item-${loop.index} ${entry.category} item-popular hide">
                    </c:otherwise>
                </c:choose>
                ${entry.content}
                </li>
            </c:forEach>
        </ul>
    </div>
</section>
