<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<section class="others" id="others">
    <div class="container">
        <h2>Other destinations</h2>
        <ul>
            <c:forEach items="${entries}" var="entry" varStatus="loop">
                <c:choose>
                    <c:when test="${loop.index < 3}">
                        <li class="item-${loop.index} ${entry.category} item-other">
                    </c:when>
                    <c:otherwise>
                        <li class="item-${loop.index} ${entry.category} item-other hide">
                    </c:otherwise>
                </c:choose>
                ${entry.content}
                </li>
            </c:forEach>
        </ul>

        <div class="text-center">
            <button type="button" id="moreOtherButton" class="link" onclick="loadMore('other', 3, 'moreOtherButton')">
                Load more <i class="fa fa-arrow-down" aria-hidden="true"></i>
            </button>
        </div>

    </div>
</section>