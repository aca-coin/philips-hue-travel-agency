package be.aca.coin.rule;

import com.liferay.content.targeting.anonymous.users.model.AnonymousUser;
import com.liferay.content.targeting.api.model.BaseFreemarkerRule;
import com.liferay.content.targeting.api.model.Rule;
import com.liferay.content.targeting.exception.InvalidRuleException;
import com.liferay.content.targeting.model.RuleInstance;
import com.liferay.content.targeting.rule.categories.BehaviorRuleCategory;
import com.liferay.content.targeting.rule.score.points.service.ScorePointLocalService;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Component(immediate = true, service = Rule.class)
public class ScorePointsMaximumRule extends BaseFreemarkerRule {

	private static final String PROPERTY_COMPANY_ID = "companyId";
	private static final String PROPERTY_ANONYMOUS_USER_ID = "anonymousUserId";
	private static final String PROPERTY_USER_SEGMENT_ID = "userSegmentId";
	private static final String PROPERTY_POINTS = "points";

	private static final String RULE_NAME = "Score Points Maximum";
	private static final String RULE_SUMMARY = "Users belong to this segment if the number of segment points is equal to the <strong>maximum score</strong> of all the user's segments.";

	@Reference private ScorePointLocalService service;

	public boolean evaluate(HttpServletRequest httpServletRequest, RuleInstance ruleInstance, AnonymousUser anonymousUser) throws Exception {
		long segmentId = ruleInstance.getUserSegmentId();
		long userId = anonymousUser.getAnonymousUserId();
		long companyId = ruleInstance.getCompanyId();

		long max = getMax(companyId, userId);
		long current = getCurrent(companyId, userId, segmentId);

		return current >= max;
	}

	private long getMax(long companyId, long anonymousUserId) {
		DynamicQuery dynamicQuery = service.dynamicQuery()
				.add(RestrictionsFactoryUtil.eq(PROPERTY_COMPANY_ID, companyId))
				.add(RestrictionsFactoryUtil.eq(PROPERTY_ANONYMOUS_USER_ID, anonymousUserId))
				.setProjection(ProjectionFactoryUtil.max(PROPERTY_POINTS));

		return calculateQueryResult(dynamicQuery);
	}

	private long getCurrent(long companyId, long anonymousUserId, long userSegmentId) {
		DynamicQuery dynamicQuery = service.dynamicQuery()
				.add(RestrictionsFactoryUtil.eq(PROPERTY_COMPANY_ID, companyId))
				.add(RestrictionsFactoryUtil.eq(PROPERTY_ANONYMOUS_USER_ID, anonymousUserId))
				.add(RestrictionsFactoryUtil.eq(PROPERTY_USER_SEGMENT_ID, userSegmentId))
				.setProjection(ProjectionFactoryUtil.property(PROPERTY_POINTS));

		return calculateQueryResult(dynamicQuery);
	}

	private long calculateQueryResult(DynamicQuery dynamicQuery) {
		List<Object> objects = service.dynamicQuery(dynamicQuery);

		if (objects.isEmpty()) {
			return 0;
		} else {
			return (Long) objects.get(0);
		}
	}

	public String processRule(PortletRequest portletRequest, PortletResponse portletResponse, String s, Map<String, String> map) throws InvalidRuleException {
		JSONObject object = JSONFactoryUtil.createJSONObject();

		return object.toJSONString();
	}

	public String getSummary(RuleInstance ruleInstance, Locale locale) {
		return RULE_SUMMARY;
	}

	@Override
	public String getName(Locale locale) {
		return RULE_NAME;
	}

	@Override
	public String getDescription(Locale locale) {
		return RULE_SUMMARY;
	}

	@Override
	public String getRuleCategoryKey() {
		return BehaviorRuleCategory.KEY;
	}
}
