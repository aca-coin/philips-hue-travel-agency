## Light up your life with Liferay and Philips Hue

In the 21st century the IoT is actually happening. Even in the world of retail stores colours are used to create engaging experiences and persuade customers towards a sale. 
Philips Hue is the next step in smart and efficient lighting. Together with the Audience Targeting functionality of Liferay, you can truly integrate into the world of the customer.

At ACA we have developed an example case where Audience Targeting meets Philips Hue to empower the sales of a travel agency.
This is the code used for the Liferay DevCon 2017 presentation which explores the possibility of integrating Audience Targeting with Philips Hue lightning. Spectators will leave the presentation with a sweet notion on how to perform the integrations themselves, learning from code examples and a live demo.
The presentation can be seen on [Youtube](https://youtu.be/6sx8vutPGm0).
The slides can be downloaded [here](https://www.liferay.com/documents/231427850/231702423/Light%20up%20your%20life%20with%20Liferay%20and%20Philips%20Hue.pdf/1127ed24-e30f-ae2e-d835-83e2fd7265c0?download=true).