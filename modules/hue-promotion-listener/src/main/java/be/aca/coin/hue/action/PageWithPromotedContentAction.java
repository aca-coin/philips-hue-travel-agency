package be.aca.coin.hue.action;

import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;

import be.aca.coin.hue.service.HueService;

@Component(
		immediate = true,
		property = {
				"key=servlet.service.events.post"
		},
		service = LifecycleAction.class
)
public class PageWithPromotedContentAction implements LifecycleAction {

	private static final Log LOG = LogFactoryUtil.getLog(PageWithPromotedContentAction.class);

	@Reference private HueService hueService;

	public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {
		if (isDisplayingPromotionalAsset(lifecycleEvent.getRequest())) {
			hueService.startBlinking();
		} else {
			hueService.stopBlinking();
		}
	}

	private boolean isDisplayingPromotionalAsset(HttpServletRequest request) {
		AssetEntry detailEntry = (AssetEntry) request.getAttribute(WebKeys.LAYOUT_ASSET_ENTRY);
		return detailEntry != null && detailEntry.getCategories().stream().anyMatch(category -> "promotion".equals(category.getTitleCurrentValue()));
	}
}