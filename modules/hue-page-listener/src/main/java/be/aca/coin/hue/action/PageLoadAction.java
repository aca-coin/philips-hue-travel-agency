package be.aca.coin.hue.action;

import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.content.targeting.model.UserSegment;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.BaseFilter;

import be.aca.coin.hue.service.HueService;
import be.aca.coin.segment.service.UserSegmentService;

@Component(
		immediate = true,
		property = {
				"servlet-context-name=",
				"servlet-filter-name=Analytics Tracker Filter",
				"url-pattern=/o/analytics-processor/track"
		},
		service = Filter.class
)
public class PageLoadAction extends BaseFilter {

	private static final Log LOGGER = LogFactoryUtil.getLog(PageLoadAction.class);

	@Reference private UserSegmentService userSegmentService;
	@Reference private HueService hueService;

	protected void processFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws Exception {
		filterChain.doFilter(request, response);

		LOGGER.info("Inside Analytics Tracker Filter: " + request.getRequestURI());

		Thread.sleep(500); // Necessary to bypass the first slow query to the table

		List<UserSegment> userSegments = userSegmentService.getUserSegments(request, response);

		if (userSegments.size() != 1) {
			return;
		}

		UserSegment userSegment = userSegments.get(0);

		LOGGER.info("Current user segment is: " + userSegment.getNameCurrentValue());

		String rgbConcat = userSegment.getDescriptionCurrentValue();

		if (rgbConcat != null && !rgbConcat.isEmpty()) {
			String[] rgb = rgbConcat.split(",");
			hueService.setRgbColor(Integer.parseInt(rgb[0]), Integer.parseInt(rgb[1]), Integer.parseInt(rgb[2]));
		}
	}

	protected Log getLog() {
		return LOGGER;
	}
}