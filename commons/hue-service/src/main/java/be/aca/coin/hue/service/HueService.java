package be.aca.coin.hue.service;

public interface HueService {

	/**
	 * Connects to the Hue.
	 */
	void connect();

	/**
	 * Toggle all lights in the default group on (true) of off (false).
	 * Brightness and color are NOT reset in the process.
	 * @param on true for ON, false for OFF.
	 */
	void toggleLights(boolean on);

	/**
	 * Sets the brightness of all lights in the default group.
	 * @param brightness value from 0 (off) to 254 (brightest).
	 */
	void setBrightness(int brightness);

	/**
	 * Sets color of all lights in the default group to the given RGB color
	 * @param r Red (0-256)
	 * @param g Green (0-256)
	 * @param b Blue (0-256)
	 */
	void setRgbColor(int r, int g, int b);

	/**
	 * Sets color of all lights in the default group to a random color
	 */
	void setRandomColor();

	/**
	 * Sets color of all lights in the default group to the given hex color (unstable!!)
	 * @param hexCode E.g. #ff0000
	 */
	void setHexColor(String hexCode);

	/**
	 * Continuously start blinking all lights in the default group in the current color.
	 */
	void startBlinking();

	/**
	 * Stop blinking all lights in the default group.
	 */
	void stopBlinking();

	/**
	 * Disconnects from Hue.
	 */
	void disconnect();
}
