package be.aca.coin.hue.service.internal;

import java.util.List;
import java.util.Random;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.philips.lighting.hue.sdk.PHAccessPoint;
import com.philips.lighting.hue.sdk.PHBridgeSearchManager;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.hue.sdk.PHSDKListener;
import com.philips.lighting.hue.sdk.utilities.PHUtilities;
import com.philips.lighting.hue.sdk.utilities.impl.Color;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLightState;

import be.aca.coin.hue.service.HueService;

@Component(
		immediate = true,
		service = HueService.class)
public class DefaultHueService implements HueService {

	private static final int MAX_RETRIES = 5;
	private static final Log LOG = LogFactoryUtil.getLog(DefaultHueService.class);
	private static final String EMULATOR_BRIDGE_IP = "127.0.0.1:80";
	private static final String EMULATOR_BRIDGE_USERNAME = "newdeveloper";
	private static final String BRIDGE_IP = "192.168.10.2";
	private static final String BRIDGE_USERNAME = "rnWl9SpC9QAY-jqFOzr0i8M4XZQtVTXZhduZaN0H";

	private PHHueSDK phHueSDK;

	private PHSDKListener listener = new PHSDKListener() {

		public void onAccessPointsFound(List accessPoint) {
			if (accessPoint.size() == 1) {
				phHueSDK.connect((PHAccessPoint) accessPoint.get(0));
			}
		}

		public void onCacheUpdated(List cacheNotificationsList, PHBridge bridge) {}

		public void onBridgeConnected(PHBridge b, String username) {
			LOG.info("Connected to bridge with username: " + username);
			phHueSDK.setSelectedBridge(b);
		}

		public void onAuthenticationRequired(PHAccessPoint accessPoint) {
			LOG.info("Waiting for you to push the bridge button... (IP " + accessPoint.getIpAddress() + ")");
			phHueSDK.startPushlinkAuthentication(accessPoint);
		}

		public void onConnectionResumed(PHBridge bridge) {}

		public void onConnectionLost(PHAccessPoint accessPoint) {}

		public void onError(int code, final String message) {}

		public void onParsingErrors(List parsingErrorsList) {}
	};

	@Activate
	public void connect() {
		phHueSDK = PHHueSDK.create();

		PHAccessPoint lastAccessPoint = new PHAccessPoint();
		lastAccessPoint.setIpAddress(BRIDGE_IP);
		lastAccessPoint.setUsername(BRIDGE_USERNAME);
		phHueSDK.connect(lastAccessPoint);

		// Register the PHSDKListener to receive callbacks from the bridge.
		phHueSDK.getNotificationManager().registerSDKListener(listener);

		PHBridgeSearchManager sm = (PHBridgeSearchManager) phHueSDK.getSDKService(PHHueSDK.SEARCH_BRIDGE);
		sm.search(true, true);

		int retries = 0;
		while (phHueSDK.getSelectedBridge() == null && retries < MAX_RETRIES) {
			sleep(1000);
			retries++;
		}

		if (retries == MAX_RETRIES) {
			throw new IllegalStateException("Cannot find Hue bridge");
		}
	}

	public void toggleLights(boolean on) {
		PHBridge bridge = phHueSDK.getSelectedBridge();
		PHLightState lightState = new PHLightState();
		lightState.setOn(on);

		bridge.setLightStateForDefaultGroup(lightState);
	}

	public void setBrightness(int brightness) {
		PHBridge bridge = phHueSDK.getSelectedBridge();
		PHLightState lightState = new PHLightState();

		lightState.setBrightness(brightness);

		bridge.setLightStateForDefaultGroup(lightState);
	}

	public void setRgbColor(int r, int g, int b) {
		float[] xy = PHUtilities.calculateXYFromRGB(r, g, b, " ");

		setColor(xy);
	}

	public void setRandomColor() {
		Random r = new Random();
		setRgbColor(r.nextInt(256), r.nextInt(256), r.nextInt(256));
	}

	public void setHexColor(String hexCode) {
		int color = Color.parseColor(hexCode); // this is an impl class!
		float[] xy = PHUtilities.calculateXY(color, " ");

		setColor(xy);
	}

	private void setColor(float[] xy) {
		PHLightState lightState = new PHLightState();
		lightState.setX(xy[0]);
		lightState.setY(xy[1]);

		PHBridge bridge = phHueSDK.getSelectedBridge();
		bridge.setLightStateForDefaultGroup(lightState);
	}

	public void startBlinking() {
		PHBridge bridge = phHueSDK.getSelectedBridge();
		PHLightState lightState = new PHLightState();

		lightState.setAlertMode(PHLight.PHLightAlertMode.ALERT_LSELECT);
		bridge.setLightStateForDefaultGroup(lightState);
	}

	public void stopBlinking() {
		PHBridge bridge = phHueSDK.getSelectedBridge();
		PHLightState lightState = new PHLightState();

		lightState.setAlertMode(PHLight.PHLightAlertMode.ALERT_NONE);
		bridge.setLightStateForDefaultGroup(lightState);
	}

	@Deactivate
	public void disconnect() {
		LOG.info("Disconnecting from bridge...");
		PHBridge bridge = phHueSDK.getSelectedBridge();
		phHueSDK.disconnect(bridge);
		phHueSDK = null;
	}

	private void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			LOG.error(e);
		}
	}
}