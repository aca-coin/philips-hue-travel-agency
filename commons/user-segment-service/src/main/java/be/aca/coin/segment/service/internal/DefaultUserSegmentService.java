package be.aca.coin.segment.service.internal;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.content.targeting.anonymous.users.model.AnonymousUser;
import com.liferay.content.targeting.anonymous.users.util.AnonymousUsersManager;
import com.liferay.content.targeting.api.model.RulesEngine;
import com.liferay.content.targeting.model.UserSegment;
import com.liferay.content.targeting.service.UserSegmentLocalService;
import com.liferay.content.targeting.util.ContentTargetingUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.GroupConstants;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.WebKeys;

import be.aca.coin.segment.service.UserSegmentService;

@Component(immediate = true, service = UserSegmentService.class)
public class DefaultUserSegmentService implements UserSegmentService {

	@Reference private AnonymousUsersManager anonymousUsersManager;
	@Reference private UserSegmentLocalService userSegmentLocalService;
	@Reference private RulesEngine rulesEngine;
	@Reference private GroupLocalService groupLocalService;
	@Reference private Portal portal;

	public List<UserSegment> getUserSegments(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		AnonymousUser anonymousUser = anonymousUsersManager.getAnonymousUser(httpServletRequest, httpServletResponse);
		long[] groupIds = getGroupIds();

		return getMatchesUserSegmentIds(httpServletRequest, groupIds, anonymousUser);
	}

	private long[] getGroupIds() throws PortalException, SystemException {
		long companyId = portal.getDefaultCompanyId();
		long groupId = groupLocalService.getGroup(companyId, GroupConstants.GUEST).getGroupId();

		return ContentTargetingUtil.getAncestorsAndCurrentGroupIds(groupId);
	}

	private List<UserSegment> getMatchesUserSegmentIds(HttpServletRequest request, long[] groupIds, AnonymousUser anonymousUser) throws Exception {

		if (ArrayUtil.isEmpty(groupIds)) {
			return null;
		}

		List<UserSegment> userSegments = userSegmentLocalService.getUserSegments(groupIds);

		return userSegments.stream()
				.filter(segment -> matches(request, anonymousUser, segment))
				.collect(Collectors.toList());
	}

	public boolean matches(HttpServletRequest request, AnonymousUser anonymousUser, UserSegment userSegment) {
		return rulesEngine.matches(request, anonymousUser, userSegment.getRuleInstances());
	}
}
