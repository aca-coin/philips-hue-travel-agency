package be.aca.coin.segment.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.content.targeting.model.UserSegment;

public interface UserSegmentService {

	List<UserSegment> getUserSegments(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception;
}
